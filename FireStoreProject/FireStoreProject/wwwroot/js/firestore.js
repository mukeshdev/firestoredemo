﻿firebase.initializeApp({
    apiKey: 'AIzaSyBihlS4zUzWLM7yx6pohRZGGG6fenAxYWQ',
    authDomain: 'audit-dev-b9444.firebaseapp.com',
    projectId: 'audit-dev-b9444'
});


var db = firebase.firestore();

function addUsers(data) {
    db.collection("users").add(data)
        .then((result) => {
        if (!result.value) {
            window.location.href = "/users"
        }
    })
        .catch(function (error) {
            swal("Good job!", "Error adding document!", "error");  
    });
}

function getUsers() {
    var trList = "";
    var i = 0;
   var list= db.collection("users").get().then((querySnapshot) => {
       querySnapshot.forEach((doc) => {
           i++;
            trList += `<tr style="cursor:pointer" onclick="getUserDetail('${doc.id}')"><th scope="row">${i}</th>
                        <td>${doc.data().firstName}</td>
                        <td>${doc.data().lastName}</td>
                        <td>${doc.data().email}</td>
                        <td><a class="btn btn-primary" href="/userEdit?id=${doc.id}">Edit</a>
                        <a class="btn btn-primary" href="javascript:void(0)" onclick="deleteRecord('${doc.id}')">Delete</a></td>
                        </tr>`;
        });
        return trList;
    });
    return list;
}

function getUserDetail(id) {
   
  db.collection("users").get().then((querySnapshot) => {
     querySnapshot.forEach((doc) => {
          if (doc.id == id) {
              $('.modal-body').html(`
            <div class=" profile">
              <div class="left-content">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7Lvm8NKK495L4AkQYR30rmuZlzRX4RmhBg4AXyggdw-INYJwP" alt="avatar">
              </div>
              <div class="right-content">
                <span class="greeting">Hello</span>
                <h2 class="my-name">
                  I'm <span>${doc.data().firstName + ' '}</span>
                </h2>
                <span class="my-website"><a href="${doc.data().url}">${doc.data().url}</a></span>
                 <div class="detail-infor">
                <div class="labels">
                  <p>Full Name:</p>
                  <p>Email:</p>
                  <p>Contact:</p>
                  <p>Company:</p>
                   <p>Url:</p>
                </div>
                <div class="infor">
                  <p>${doc.data().firstName +' '+ doc.data().lastName}</p>
                  <p>${doc.data().email}</p>
                 <p>${doc.data().phone}</p>
                 <p>${doc.data().company}</p>
                <p>${doc.data().url}</p>

                </div>
              </div>
              </div>
             <div class="bottom-content">
               <a href="#"><i class="fab fa-facebook-square"></i></a>
               <a href="#"><i class="fab fa-codepen"></i></a>
               <a href="#"><i class="fab fa-google"></i></a>
               <a href="#"><i class="fab fa-twitter"></i></a>
                 <a href="#"> <i class="fab fa-github"></i></a>
                 </div>
            </div>`);
                          $('#myModal').modal('show');  
                      }
                    });
     
    });


}

function getUser(id) {
    var rec;
    var reccord = db.collection("users").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            if (doc.id == id) {
                rec = doc.data();
            }
        });
        return rec;
    });
    return reccord;
}

function updateUser(data,id) {
    var userRef = db.collection("users");

    userRef.doc(id).set({
        firstName: data.firstName, lastName: data.lastName, company: data.company,
        phone: data.phone, email: data.email,
        url: data.url
    }).then(function (docRef) {
        swal("Good job!", "Document updated sucessfully!", "success").then((result) => {
            if (!result.value) {
                window.location.href = "/users"
            }
        })
    })
        .catch(function (error) {
            swal("Good job!", "Error updating document!", "error");
        });
}

function deleteRecord(id) {
    var userRef = db.collection("users");

    userRef.doc(id).delete().then(function (docRef) {
        swal("Good job!", "Document delete sucessfully!", "success").then((result) => {
            if (!result.value) {
                window.location.href = "/users"
            }
        })
    })
        .catch(function (error) {
            swal("Good job!", "Error deleting document!", "error");
        });
}
